import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * 
 * -> Class: Data Structures - 2720 Lab:
 * -> LAB: 03 [Solution]
 * -> Date: Friday 07 Sep, 2018
 * -> Subject: Java Revision (Recursion)
 * -> Lab Web-page: [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/]
 */
public class Main {

	public static void main(String[] args) {
		
		Functions fun = new Functions();
		int sum = 0;
		long startTime, endTime;
		
		/* ---- Fun 1: Add [Recursively] ---- */
		// TASK:
		// 1. Call the method "addRec" for a = 10, b = 3,
		// 2. Print the result. Is that correct?
		sum = fun.addRec(10, 3);
		System.out.println("SUM:" + sum);
		/* ---- End Fun 1 ---- */
		
		
		
		/* ---- Experiment I: Recursive ---- */
		// TASK:
		// 1. Run 'addRec' where a = 100,000 and b = 3,
		// 2. Run 'addRec' where a = 3, and b = 100,000,
		// Q. What do you observe? Why do you think this happened?
		//TODO
		sum = fun.addRec(100, 3);
		sum = fun.addRec(3, 100);
		/* ---- End Experiment I ---- */
		
		
		
		
		/* ---- Fun 2: Sum [Iterative] ---- */
		// TASK:
		// 1. Call the method "sum" for n = 100,
		// 2. Print the result. Is that correct?
		sum = fun.sum(100);
		System.out.println("SUM [Iterative] = " + sum);
		/* ---- End Fun 2 ---- */
		
		
		
		/* ---- Fun 3: Sum [Recursive] ---- */
		// TASK:
		// 1. Call the method "sumRec" for n = 100,
		// 2. Print the result. Is that correct?
		sum = fun.sumRec(100);
		System.out.println("SUM [Recursive] = " + sum);
		/* ---- End Fun 3 ---- */
		
		
		
		/* ---- Experiment II: Iterative VS Recursive ---- */
		// TASK:
		// 1. Record the execution time for "sum",
		// 2. Record the execution time for "sumRec",
		// 3. Compare the execution time as you increase n,
		// Q. Which one is faster?
		// Q. Which one is able to deal with very large values for n?
		startTime = System.nanoTime();
		sum = fun.sum(100);
		endTime = System.nanoTime();
		System.out.println("SUM [Iterative] = " + sum + "\tin [" + (endTime - startTime) + "] miliseconds.");
		
		startTime = System.nanoTime();
		sum = fun.sumRec(100);
		endTime = System.nanoTime();
		System.out.println("SUM [Recursive] = " + sum + "\tin [" + (endTime - startTime) + "] miliseconds.");
		/* ---- End Experiment II ---- */
		
		
		
		/* ---- Fun 4: search [Iterative] ---- */
		// TASK:
		// 1. Call the method "search" where arr = {12,4,7,33,21,54,9,10,17} and x = 33.
		// 2. Print the result. Is that correct?
		// 3. Call it again for the same arr but x = 13.
		// 4. Print the result. Do you get (-1)?
		long[] arr = {12,4,7,33,21,54,9,10,17};
		int res = fun.search(arr, 33);
		System.out.println("Search --> Found at = " + res);
		/* ---- End Fun 4 ---- */
		

		
		/* ---- Fun 5: searchByDividing [Iterative] ---- */
		// TASK:
		// 1. Call the method "searchByDividing" where arr = {4,7,9,10,12,17,21,33,54} and x = 33.
		// 2. Print the result. Is that correct?
		// 3. Call it again for the same arr but x = 13.
		// 4. Print the result. Do you get (-1)?
		arr = new long[]{4,7,9,10,12,17,21,33,54};
		res = (int)fun.searchByDividing(arr,0, arr.length - 1, 33);
		/* ---- End Fun 5 ---- */
		
		
		
		/* ---- Fun 6: searchByDividingRec [Recursive] ---- */
		// TASK:
		// 1. Call the method "searchByDividingRec" where arr = {4,7,9,10,12,17,21,33,54} and x = 33.
		// 2. Print the result. Is that correct?
		// 3. Call it again for the same arr but x = 13.
		// 4. Print the result. Do you get (-1)?
		arr = new long[]{4,7,9,10,12,17,21,33,54};
		res = (int)fun.searchByDividingRec(arr,0, arr.length - 1, 33);
		System.out.println("The queried value is at position [" + res + "].");
		/* ---- End Fun 6 ---- */
		
		
		
		/* ---- Experiment III: Iterative VS Recursive ---- */
		// TASK:
		// 1. Run 'search' where arr = {1,2,3,... ,100000} and x =  99999.
		// 2. Run 'search' for the same arr and x.
		// 3. Run 'search' for the same arr and x.
		// Q. Which one is faster?

		int n = 100000;
		long x = 99999;
		int loc = 0;
		long[] arr2 = new long[n];
		for(int i = 0; i < arr2.length; i++) {
			arr2[i] = i;
		}
		
		startTime = System.nanoTime();
		loc = fun.search(arr2, x);
		endTime = System.nanoTime();
		System.out.println("Found by [Stupid Search] at " + loc + "\tin [" + (endTime - startTime) + "] miliseconds.");

		startTime = System.nanoTime();
		loc = fun.searchByDividingRec(arr2, 0, n-1, x);
		endTime = System.nanoTime();
		System.out.println("Found by [Recursive Search] at " + loc + "\tin [" + (endTime - startTime) + "] miliseconds.");
		
		
		startTime = System.nanoTime();
		loc = fun.searchByDividing(arr2, 0, n-1, x);
		endTime = System.nanoTime();
		System.out.println("Found by [Iterative Search] at " + loc + "\tin [" + (endTime - startTime) + "] miliseconds.");
		/* ---- End Experiment III ---- */
		
		
		/* ---- Fun 5: searchByDividing [Iterative] ---- */
		// TASK:
		// 1. Create a list called 'line' of 6 zeros.
		// 2. Call the method 'howManyInLine' for 'line' and 0.
		// Q. Is the output correct? Did you get all numbers from 2 to 6?
		n = 0;
		List<Integer> line = new LinkedList<>();
		Collections.addAll(line, 0, 0, 0, 0, 0);
		
		res = fun.howManyInLine(line, n);
		/* ---- End Fun 5 ---- */
	}

}
