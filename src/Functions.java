import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import interfaces.IFunctions;

public class Functions implements IFunctions {

	@Override
	public int addRec(int a, int b) {
		if (b == 0)
			return a;
		return addRec(a + 1, b - 1);
	}

	@Override
	public int sum(int n) {

		int sum = 0;
		for (int i = 1; i <= n; i++) {
			sum += i;
		}
		return sum;
	}

	@Override
	public int sumRec(int n) {

		if (n == 1) {
			return 1;
		} else {
			return (n + sumRec(n - 1));
		}
	}

	@Override
	public int search(long[] arr, long x) {

		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == x)
				return i;
		}
		return -1;
	}

	@Override
	public int searchByDividingRec(long[] arr, int from, int to, long x) {

		if (from > to) {
			return -1;
		}

		int middle = (from + to) / 2;

		if (arr[middle] == x)
			return middle;
		else if (arr[middle] < x)
			return searchByDividingRec(arr, middle + 1, to, x);
		else {
			return searchByDividingRec(arr, from, middle - 1, x);
		}
	}

	@Override
	public int searchByDividing(long[] arr, int from, int to, long x) {

		int middle = (from + to) / 2;

		while (from <= to) {
			if (arr[middle] < x) {
				from = middle + 1;
			} else if (arr[middle] == x) {
				return middle;
			} else {
				to = middle - 1;
			}
			middle = (from + to) / 2;
		}
		if (from > to) {
			return -1;
		}
		return -1;
	}

	@Override
	public int howManyInLine(List<Integer> line, int n) {

		// Create 'n' spaces
		String spaces = IntStream.range(0, n).mapToObj(i -> " ").collect(Collectors.joining(""));

		// Only for the first time
		if (n == 0) {
			System.out.println("How many?");
		}

		// Base condition (last person)
		if (line.size() == 1) {
			System.out.println(spaces + "Just me!");
			return 1;
		}

		// Recursive step (for every person except the last one)
		System.out.println(spaces + "Me and the guys behind me!");
		line.remove(line.size() - 1);
		int res = howManyInLine(line, ++n);
		System.out.println(spaces + (res + 1));
		return 1 + res;

	}

}
